# Project: HungerBot (A server less project)
Version: 1.0

HungerBot is a food recommendation bot which recommends the dishes based on your interest and choice of food. 

## Technologies:
* * *
* AngularJS  
* HTML  
* CSS 
* Serverless Framework
* This project runs on AWS Server less platform using different types of AWS services.  
	* Simple Storage Service  
	* CloudFront  
	* Route53  
	* API Gateway  
	* Lambda  
	* DynamoDB   
	* SageMaker  

##  Installation
* * *
* Dependencies: AWS account
* Deployed using Serverless Framework

### Repo Owner: Tech9

### Team: Boolean Pundits

### Contribution:
* * *
* Somnath Pawar  
* Aman Angira  
* Sanghmitra Shinde  
* Anshaj Khare  
* Ajay Reddy  
* Nitin Pahal  
* Sagar Mahajan  
* Deepali Bansal  
* Suhas Girgoankar  