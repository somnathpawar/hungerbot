import org.testng.Assert;
import org.testng.annotations.Test;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


@Test
public class GetDish {
	
public void VerifyGetDishApi()
{
	// Specify the base URL to the RESTful web service
	 RestAssured.baseURI = "https://xunn15hii2.execute-api.us-east-1.amazonaws.com/dev";
	 
	 // Get the RequestSpecification of the request that you want to sent
	 // to the server. The server is specified by the BaseURI that we have
	 // specified in the above step.
	 RequestSpecification httpRequest = RestAssured.given();
	 
	 JSONObject requestParams = new JSONObject();
	 requestParams.put("cuisine", "Indian"); 
	 requestParams.put("preference","veg");
	 httpRequest.body(requestParams.toJSONString());
	 httpRequest.header("x-api-key","I9UI6bVC33aSPipys43iR7kXYJcTn7gSaJ5QxKhG");
	 Response response = httpRequest.request(Method.POST, "get-dishes");
	 String responseBody = response.getBody().asString();
	 int statusCode = response.getStatusCode();
	 System.out.println("Response Code for GetDish is :" + response.statusCode());
	 System.out.println(response.asString());
	 Assert.assertEquals(statusCode, 200); 
	 System.out.println("Response Body is =>  " + responseBody);
	 
}

public void VerifyGetSimilarDishApi()
{
	// Specify the base URL to the RESTful web service
	 RestAssured.baseURI = "https://xunn15hii2.execute-api.us-east-1.amazonaws.com/dev/";
	 
	 // Get the RequestSpecification of the request that you want to sent
	 // to the server. The server is specified by the BaseURI that we have
	 // specified in the above step.
	 RequestSpecification httpRequest = RestAssured.given();
	 
	 // Make a request to the server by specifying the method Type and the method URL.
	 // This will return the Response from the server. Store the response in a variable.
	 
	 httpRequest.header("x-api-key","I9UI6bVC33aSPipys43iR7kXYJcTn7gSaJ5QxKhG");
	 Response response = httpRequest.request(Method.GET, "get-similar-dishes/1307");
	
	 int statusCode = response.getStatusCode();
	 System.out.println("Response Code for GetSimilarDish is :" + response.statusCode());
	 System.out.println(response.asString());
	 Assert.assertEquals(statusCode, 200); 
	 String responseBody = response.getBody().asString();
	 System.out.println("Response Body is =>  " + responseBody);
}

public void VerifyGetDishDetail()
{
	 RestAssured.baseURI = "https://xunn15hii2.execute-api.us-east-1.amazonaws.com/dev/";
	 RequestSpecification httpRequest = RestAssured.given();
	 httpRequest.header("x-api-key","I9UI6bVC33aSPipys43iR7kXYJcTn7gSaJ5QxKhG");
	 Response response = httpRequest.request(Method.GET, "get-similar-dishes/1307");
	 int statusCode = response.getStatusCode();
	 System.out.println("Response Code for GetDishDetail :" + response.statusCode());
	 System.out.println(response.asString());
	 Assert.assertEquals(statusCode, 200); 
	 String responseBody = response.getBody().asString();
	 System.out.println("Response Body is =>  " + responseBody);
}
}
