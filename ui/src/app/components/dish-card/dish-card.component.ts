import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: '.dish-card',
  templateUrl: './dish-card.component.html',
  styleUrls: ['./dish-card.component.scss']
})
export class DishCardComponent implements OnInit {

  // Dish card to display similar dishes in slider

  @Input() data;

  constructor(
    public DataService: DataService
  ) { }

  ngOnInit(): void {
  }

}
