import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data/data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: '.app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  results: any = [];
  data: any = {};
  loading = true;
  readMore = false;

  constructor(
    public DataService: DataService,
    private HttpClient: HttpClient
  ) { }

  isReadMore(description) {
    if (description.scrollHeight > 80) {
      return true;
    }
    return false;
  }

  ngOnInit(): void {

    // Get all the data based on users choice

    this.data = this.DataService.getCompleteData();
    this.HttpClient.post('https://xunn15hii2.execute-api.us-east-1.amazonaws.com/dev/get-dishes', {
      "cuisine": this.data.cuisine,
      "preference": this.data.type
    }).subscribe(data => {
      this.results = data;
      this.loading = false;
    });
  }

}
