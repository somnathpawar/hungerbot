import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ConfigService } from '../../services/config/config.service';
import { DomSanitizer } from '@angular/platform-browser';
import { DataService } from '../../services/data/data.service';

@Component({
  selector: '.app-questionnaire',
  templateUrl: './questionnaire.component.html',
  styleUrls: ['./questionnaire.component.scss']
})
export class QuestionnaireComponent implements OnInit {

  cuisineFormGroup: FormGroup;
  foodTypeFormGroup: FormGroup;
  animation = '';

  constructor(
    private FormBuilder: FormBuilder,
    public ConfigService: ConfigService,
    private DomSanitizer: DomSanitizer,
    private DataService: DataService
  ) { }

  getImageUrl(imageUrl) { return this.DomSanitizer.bypassSecurityTrustStyle(`url(${imageUrl}), rgba(255, 255, 255, 0.7)`) }

  ngOnInit(): void {
    let data = this.DataService.getCompleteData();

    this.cuisineFormGroup = this.FormBuilder.group({
      cuisine: [data.cuisine]
    });

    this.foodTypeFormGroup = this.FormBuilder.group({
      foodType: [data.type]
    });

    // save data to localStorage on change
    this.cuisineFormGroup.get('cuisine').valueChanges.subscribe(value => this.DataService.saveCuisine(value));
    this.foodTypeFormGroup.get('foodType').valueChanges.subscribe(value => this.DataService.saveFoodType(value));
  }

}
