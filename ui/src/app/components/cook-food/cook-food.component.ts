import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data/data.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-cook-food',
  templateUrl: './cook-food.component.html',
  styleUrls: ['./cook-food.component.scss']
})
export class CookFoodComponent implements OnInit {

  id;
  dish;
  similarDishes = [];
  loading = true;
  readMore = false;

  constructor(
    private HttpClient: HttpClient,
    private ActivatedRoute: ActivatedRoute,
    public DataService: DataService
  ) { }

  loadData() {

    //  Get details for the dish selected
    this.HttpClient.get(
      `https://xunn15hii2.execute-api.us-east-1.amazonaws.com/dev/get-dish-details/${this.id}`
    ).subscribe(
      res => {
        this.dish = res[0];
        this.loading = false;
      }
    );

    // Get similar dishes
    this.HttpClient.get(
      `https://xunn15hii2.execute-api.us-east-1.amazonaws.com/dev/get-similar-dishes/${this.id}`
    ).subscribe(
      (res: any) => {
        this.similarDishes = res;
      }
    );
  }

  isReadMore(description) {
    if (description.scrollHeight > 80) {
      return true;
    }
    return false;
  }

  ngOnInit(): void {
    this.ActivatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.loadData();
    })
  }

}
