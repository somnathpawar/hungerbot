import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CookFoodComponent } from './cook-food.component';

describe('CookFoodComponent', () => {
  let component: CookFoodComponent;
  let fixture: ComponentFixture<CookFoodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CookFoodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CookFoodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
