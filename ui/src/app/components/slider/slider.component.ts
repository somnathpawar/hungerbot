import { AfterViewInit, Component, ContentChildren, Directive, ElementRef, Input, OnInit, QueryList, TemplateRef, ViewChild, ViewChildren } from '@angular/core';
import { SliderItemDirective } from '../../directives/slider/slider-item.directive';
import { animate, AnimationBuilder, AnimationFactory, AnimationPlayer, style } from '@angular/animations';

@Directive({
  selector: '.slider-item'
})
export class SliderItemElementDirective {
}

@Component({
  selector: 'slider',
  exportAs: 'slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements AfterViewInit {

  /*
    Component which renders templates from slider-item-directive as slides as slide them one at a time
  */

  @ContentChildren(SliderItemDirective) items: QueryList<SliderItemDirective>; // Getting all instances of slider-item-directive used under SliderComponent Instance
  @ViewChildren(SliderItemElementDirective, { read: ElementRef }) private itemsElements: QueryList<ElementRef>; // Getting all instances of SliderItemElement generated in place of SliderItemDirective
  @ViewChild('slider') private slider: ElementRef; // Element reference of current commponent 
  @Input() timing = '250ms ease-in'; // Animation timing
  @Input() showControls = true; // to hide and show slider arrows if required
  private player: AnimationPlayer;
  private itemWidth: number; // width of a single item in slider
  currentSlide = 0; // current slide in the starting of slider

  constructor(private builder: AnimationBuilder) {
  }

  // Next and previous functions to slide items

  next() {
    if (this.currentSlide + 4 >= this.items.length) return; // checking if more slider are available at the end
    
    this.currentSlide = (this.currentSlide + 1) % this.items.length;
    const offset = this.currentSlide * this.itemWidth; // setting the offset width to slide exactly one item

    //  Slide animation
    const myAnimation: AnimationFactory = this.buildAnimation(offset);
    this.player = myAnimation.create(this.slider.nativeElement);
    this.player.play();
  }

  prev() {
    if (this.currentSlide === 0) return;  // checking if more slider are available at the end

    this.currentSlide = ((this.currentSlide - 1) + this.items.length) % this.items.length;
    const offset = this.currentSlide * this.itemWidth;

    const myAnimation: AnimationFactory = this.buildAnimation(offset);
    this.player = myAnimation.create(this.slider.nativeElement);
    this.player.play();
  }

  private buildAnimation(offset) {

    // Sliding animation
    
    return this.builder.build([
      animate(this.timing, style({ transform: `translateX(-${offset}px)` }))
    ]);
  }

  ngAfterViewInit() {

    // Setting the itemWidth after all elements are rendered in the DOM.
    
    setTimeout(() => {
      this.itemWidth = this.itemsElements.first && this.itemsElements.first.nativeElement.getBoundingClientRect().width;
    });
  }

}
