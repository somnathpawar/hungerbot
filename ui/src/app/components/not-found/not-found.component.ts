import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/services/data/data.service';

@Component({
  selector: '.not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  // Component to display that revelant data not found

  @Input() data;

  constructor(
    public DataService: DataService
  ) { }

  ngOnInit(): void {
  }

}
