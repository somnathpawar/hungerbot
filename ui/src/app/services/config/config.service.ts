import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  // Initial options for getting preferrences from user

  cuisineOptions = [
    {
      name: 'Indian',
      value: 'Indian',
      image: 'https://ichef.bbci.co.uk/news/410/cpsprodpb/E982/production/_109887795_gettyimages-153775715-1.jpg'
    },
    {
      name: 'Mexican',
      value: 'Mexican',
      image: 'https://www.iloveqatar.net/public/images/slider/_760x500_clip_center-center_none/Top-five-spots-for-Mexican-food-in-Qatar.jpg'
    },
    {
      name: 'Chinese',
      value: 'Chinese',
      image: 'https://ez2eat.s3.amazonaws.com/RE1574268219/background_images/0.jpg?v=6.8.14'
    },
    {
      name: 'Thai',
      value: 'Thai',
      image: 'https://www.englishclub.com/images/vocabulary/food/thai/thai-food.jpg'
    },
    {
      name:  'French',
      value:  'French',
      image:  'https://www.kids-world-travel-guide.com/images/xfrench_food_macarons_shutterstock_62967172-2.jpg.pagespeed.ic.exomk2uTXs.jpg',
    },
    {
      name:  'Italian',
      value:  'Italian',
      image:  'https://i.ndtvimg.com/i/2015-12/italian_625x350_41450863014.jpg',
    },
    {
      name:  'Japanese',
      value:  'Japanese',
      image:  'https://upload.wikimedia.org/wikipedia/commons/5/57/Oseti.jpg',
    },
    {
      name:  'Arabic',
      value:  'Arabic',
      image:  'https://i.ndtvimg.com/i/2016-05/arabic-food_625x350_71463118204.jpg',
    },
    {
      name:  'Moroccon',
      value:  'Moroccon',
      image:  'https://www.luxurymaroc.com/uploads/inspire_category/26b23c37b1f0108c6f0885b975235c44.jpg',
    }
  ];

  foodTypeOptions = [
    {
      name: 'Veg',
      value: 'veg',
      image: 'https://upload.wikimedia.org/wikipedia/commons/e/e9/Soy-whey-protein-diet.jpg'
    },
    {
      name: 'Non-veg',
      value: 'nonveg',
      image: 'https://cdn.shopify.com/s/files/1/1647/4455/files/chicken.jpg?6156539831093887798'
    },
    {
      name: 'Vegan',
      value: 'vegan',
      image: 'https://www.heartfoundation.org.nz/media/images/nutrition/page-heros/plant-based-diet_737_553_c1.jpg'
    },
    {
      name: 'Egg',
      value: 'egg',
      image: 'https://i.ndtvimg.com/i/2015-11/boiled-eggs_625x350_61447756040.jpg'
    },
  ];

  constructor() { }
}
