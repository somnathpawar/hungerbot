import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  getData(data) {
    // Parsing the raw data coming from backend and converting it to an array of string so that it can be iterated
    return data.replace("['", "").replace("']", "").split("','");
  }

  getImage(image) {
    // An image path returned as url to pass it as the value of background-image css
    return `url(${image})`;
  }

  getCompleteData() {
    // Getting data from localstorage and returning it after parsing to provide a JSON object
    return JSON.parse(localStorage.getItem('data')) || {};
  }

  foodQuery(name) {
    //Creating a query string for swiggy url
    return name.replace(' ', '+');
  }


  /*
    Save functions to save cuisine and food preference in localStorage
  */

  saveCuisine(cuisine) {
    let data = JSON.parse(localStorage.getItem('data')) || {};
    data.cuisine = cuisine;
    localStorage.setItem('data', JSON.stringify(data));
  }

  saveFoodType(type) {
    let data = JSON.parse(localStorage.getItem('data')) || {};
    data.type = type;
    localStorage.setItem('data', JSON.stringify(data));
  }
}
