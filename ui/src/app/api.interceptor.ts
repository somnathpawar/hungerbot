import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<any>> {
    /*
      Intercepting every network API call to add the required authentication token
    */
    let newHeaders = request.headers;
    newHeaders = newHeaders.append('x-api-key', 'I9UI6bVC33aSPipys43iR7kXYJcTn7gSaJ5QxKhG');
    const authReq = request.clone({ headers: newHeaders });
    return next.handle(authReq);
  }
}
