import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[sliderItem]'
})
export class SliderItemDirective {

  // Used as structural directive to get all the item templates to use iterate into slider

  constructor(public tpl: TemplateRef<any>) {
  }

}
