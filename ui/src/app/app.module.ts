import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatStepperModule } from "@angular/material/stepper";

import { ResultsComponent } from './components/results/results.component';
import { QuestionnaireComponent } from './components/questionnaire/questionnaire.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatRadioModule } from '@angular/material/radio';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SliderComponent, SliderItemElementDirective } from './components/slider/slider.component';
import { SliderItemDirective } from './directives/slider/slider-item.directive';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { CookFoodComponent } from './components/cook-food/cook-food.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './api.interceptor';
import { LoaderComponent } from './components/loader/loader.component';
import { DishCardComponent } from './components/dish-card/dish-card.component';
import { WorkInProgressComponent } from './components/work-in-progress/work-in-progress.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    ResultsComponent,
    QuestionnaireComponent,
    SliderComponent,
    SliderItemDirective,
    SliderItemElementDirective,
    CookFoodComponent,
    LoaderComponent,
    DishCardComponent,
    WorkInProgressComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    MatToolbarModule,
    MatStepperModule,
    MatButtonModule,
    MatIconModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule,
    MatExpansionModule,
    MatTabsModule,
    MatCardModule,
    MatTooltipModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: '',
        component: QuestionnaireComponent
      },
      {
        path: 'results',
        component: ResultsComponent,
      },
      {
        path: 'login',
        component: WorkInProgressComponent,
      },
      {
        path: 'sign-up',
        component: WorkInProgressComponent,
      },
      {
        path: 'dish/:id',
        component: CookFoodComponent
      },
      {
        path: '**',
        redirectTo: '',
        pathMatch: 'full'
      }
    ])
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
