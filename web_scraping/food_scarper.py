import requests
import csv
import json
import logging
import re
import time
from recipe_scrapers import scrape_me

logging.basicConfig(filename='food_scraper.log', filemode='w', format='%(asctime)s [%(levelname)s]: %(message)s', level=logging.INFO)
headers = ["recipe_id", "title", "recipe_photo_url", "primary_photo_url", "record_url", "ingredients", "instructions"]
count = 0
file_name = 'scraped_data_{}.csv'.format(int(time.time()))


input_str = input("Please enter page no like '1' or range of page nos separated by hyphen(-) like '1-10' which you want to scrape: ")

# parse the inputs
input_str = input_str.split('-')
range_list = [1]
if len(input_str) > 1:
    range_1 = "".join(re.findall(r'\d+', input_str[0]))
    range_2 = "".join(re.findall(r'\d+', input_str[1]))
    if range_1 != '':
        range_list[0] = int(range_1)
    if range_2 != '':
        range_list.append(int(range_2)+1)
else:
    input_str = "".join(re.findall(r'\d+', input_str[0]))
    if input_str != '':
        range_list = [int(input_str),int(input_str)+1]

# scrape the site and write to a csv file.
with open(file_name, 'w') as file:
    writer = csv.writer(file, delimiter=',')
    writer.writerow(headers)

    for i in range(*range_list):
        url = "https://api.food.com/services/mobile/fdc/search/sectionfront?pn={}&recordType=Recipe&collectionId=17' -H 'Connection: keep-alive' -H 'Accept: application/json, text/javascript, */*; q=0.01' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36' -H 'Content-Type: application/json' -H 'Origin: https://www.food.com' -H 'Sec-Fetch-Site: same-site' -H 'Sec-Fetch-Mode: cors' -H 'Referer: https://www.food.com/recipe?ref=nav' -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-GB,en-US;q=0.9,en;q=0.8".format(i)
        result = requests.get(url)
        result = result.json()

        if result.get('response') and result['response'].get('results'):

            for item in result['response']['results']:
                row = []

                for header in headers:

                    if item.get(header):
                        row.append(str(item.get(header)))
                    else:
                        row.append("")

                    if header == "record_url":

                        try:
                            scraper = scrape_me(item.get(header))
                            row.append(str(json.dumps(scraper.ingredients())))
                            row.append(str(json.dumps(scraper.instructions())))
                        except Exception as e:
                            logging.error("Error:{} in receipt_id: {}".format(e, item.get('recipe_id')))
                            
                writer.writerow(row)
                count += 1
                logging.info("Parsed Page No: {}, Row No: {}".format(i, count))
