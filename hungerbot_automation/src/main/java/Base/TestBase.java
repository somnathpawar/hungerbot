package Base;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;
import io.github.bonigarcia.wdm.ChromeDriverManager;


public class TestBase {
	public static WebDriver driver;
	ConfigFileReader readValue;

	@BeforeMethod
	public void initializeApplication()
	{
		ChromeDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	   
		//To read value from config file
		
	    readValue = new ConfigFileReader();
	    String url = readValue.getApplicationUrl();
		driver.get(url); 
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

	}
	
	@AfterMethod
	public void applicationEnd()
	{
		driver.quit();
	}
	
	@AfterSuite
	public void sendMail()
	{
		SendTestReport testReport = new SendTestReport();
		testReport.sendTestReport();
	}
	
}
