package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import Base.TestBase;

public class FoodRecommendationPage extends TestBase {
	public WebDriver driver;

	public void SelectCuisinePage(WebDriver driver) {
		this.driver = driver;
	}

	@FindBy(xpath = "//mat-icon[contains(text(),'chevron_left')]")
	WebElement scrollLeft;

	@FindBy(xpath = "//mat-icon[contains(text(),'chevron_right')]")
	WebElement scrollRight;

	@FindBy(xpath = "//span[contains(text(),'Next')]")
	WebElement nextButton;

	@FindBy(id = "search")
	WebElement searchButton;

	@FindBy(xpath = "//mat-radio-button[@id='Indian']")
	WebElement indianCuisine;

	@FindBy(xpath = "//mat-radio-button[@id='Mexican']")
	WebElement mexicanCuisine;

	WebElement chineseCuisine;

	WebElement thaiCuisine;

	WebElement frenchCuisine;

	/**
	 * Food Preferences
	 */
	@FindBy(xpath = "//mat-radio-button[@id='veg']")
	WebElement vegPref;

	@FindBy(xpath = "//mat-radio-button[@id='nonveg']")
	WebElement nonVegPref;

	@FindBy(xpath = "//mat-radio-button[@id='mat-radio-8']//div[@class='option-name'][contains(text(),'EGG')]")
	WebElement eggPref;

	@FindBy(xpath = "//mat-radio-button[@id='mat-radio-8']//div[@class='option-name'][contains(text(),'VEGAN')]")
	WebElement veganPref;

	@FindBy(xpath = "//a[@id='dish-details-order-food-button']")
	WebElement ordersimilarButton;

	@FindBy(xpath = "/html/body/div/div/div/div/mat-horizontal-stepper/div[2]/div[3]/form/div/button[2]")
	// @FindBy(xpath = "//*[@id=\"cdk-step-content-3-2\"]/form/div/button[2]")
	WebElement notOnDietOption;

	@FindBy(xpath = "//button[@id='login-button']")
	WebElement loginButton;

	@FindBy(xpath = "//button[@id='sign-up-button']")
	WebElement signUpbutton;

	@FindBy(xpath = "//a[@id='go-to-dish-details-button']")
	WebElement loveToCookbutton;

	@FindBy(xpath = "//a[@id='results-order-food-button']")
	WebElement lazyToCookbutton;
	
	@FindBy(xpath = "//mat-expansion-panel-header[@id='mat-expansion-panel-header-1']")
	WebElement secondElement;
	
	@FindBy(xpath = "//div[@id='mat-tab-label-0-1']")
	WebElement stepsButton;
	
	@FindBy(xpath = "//a[@id='not-found-back-to-home']") 
	WebElement backToHomeButton;


	/**
	 * Method to select Cuisine
	 * 
	 * @param cuisine
	 */
	public void SelectCuisine(String cuisine) {
		// TODO Auto-generated method stub
		if (cuisine.equalsIgnoreCase("INDIAN")) {
			indianCuisine.click();
		} else if (cuisine.equalsIgnoreCase("mexican")) {
			mexicanCuisine.click();
		} else if (cuisine.equalsIgnoreCase("chinese")) {
			chineseCuisine.click();
		} else if (cuisine.equalsIgnoreCase("thai")) {
			thaiCuisine.click();
		} else if (cuisine.equalsIgnoreCase("french")) {
			frenchCuisine.click();
		}

	}

	public void waitForLoad(WebDriver driver) {
		ExpectedCondition<Boolean> pageLoadCondition = new ExpectedCondition<Boolean>() {
			public Boolean apply(WebDriver driver) {
				return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
			}
		};
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(pageLoadCondition);
	}

	/**
	 * Method to click Next button
	 */
	public void clickOnNext() {
		// TODO Auto-generated method stub
		nextButton.click();
	}

	/**
	 * Method to click Next button
	 */
	public void clickOnSearchButton() {
		// TODO Auto-generated method stub
		searchButton.click();
	}

	/**
	 * Click on Lazy to Cook Button
	 */
	public void clickonLazyToCook() {
		lazyToCookbutton.click();
	}

	/**
	 * Click on Love to Cook Button
	 */
	public void clickonLoveToCook() {
		loveToCookbutton.click();
	}

	/**
	 * Method to Select Food Preference
	 * 
	 * @param preference
	 */
	public void SelectFoodPreference(String preference) {
		// TODO Auto-generated method stub
		if (preference.equalsIgnoreCase("Veg")) {
			vegPref.click();
		} else if (preference.equalsIgnoreCase("NonVeg")) {
			nonVegPref.click();
		} else if (preference.equalsIgnoreCase("Egg")) {
			eggPref.click();
		} else if (preference.equalsIgnoreCase("Vegan")) {
			veganPref.click();
		}
	}

	/**
	 * @throws InterruptedException 
	 * 
	 */
	public void clickonLogin() throws InterruptedException {
		// TODO Auto-generated method stub
		//highLighterMethod(driver,loginButton);
		//Thread.sleep(2000);
		loginButton.click();

	}
	
	//Creating a custom function
		public void highLighterMethod(WebDriver driver, WebElement element){
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", element);
		}

	/**
	 * 
	 */
	public void clickonSignUp() {
		// TODO Auto-generated method stub
		signUpbutton.click();
	}

	public void clickSecondOption() throws InterruptedException {
		// TODO Auto-generated method stub
		secondElement.click();
		Thread.sleep(3000);

	}
	
	public void clickonSteps() {
		// TODO Auto-generated method stub
		stepsButton.click();
	}

	public void clickOnOrderSimilar() {
		// TODO Auto-generated method stub
		ordersimilarButton.click();
	}

	public void clickonBackToHomeButton() throws InterruptedException {
		// TODO Auto-generated method stub
		Thread.sleep(2000);
		backToHomeButton.click();
	}

}
