package TestCases;

import Base.ConfigFileReader;
import Base.ExtentTestManager;
import Base.TestBase;
import Pages.FoodRecommendationPage;
import junit.framework.Assert;

import org.testng.annotations.*;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;

public class OrderYourFood extends TestBase {

	ConfigFileReader readValue;
	FoodRecommendationPage foodRecommend;
	public static ExtentTest child;

	String cuisine = "indian";
	String preference = "veg";

	/**
	 * Method to Order Your Food Based on Your Preferencesss
	 *
	 * @throws Exception
	 */
	@Test
	public void orderYourFoodTest() throws Exception {

		foodRecommend = new FoodRecommendationPage();
		foodRecommend = PageFactory.initElements(driver, FoodRecommendationPage.class);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		child = ExtentTestManager.startTest("Order your food using Food Recommendation", "Order Your Food");
		foodRecommend.waitForLoad(driver);
		child.log(Status.INFO, "Select Cuisine on Select Cuisine Page");
		foodRecommend.SelectCuisine(cuisine);
		Thread.sleep(2000);
		child.log(Status.INFO, "Select Your Food Preferences");
		foodRecommend.SelectFoodPreference(preference);
		Thread.sleep(2000);
		child.log(Status.INFO, "Click on Search Button");
		foodRecommend.clickOnSearchButton();
		Thread.sleep(5000);
		String parent = driver.getWindowHandle();
		child.log(Status.INFO, "Redirecting to the next page");
		if (driver.getPageSource().contains("Food results based on your choice:")) {
			child.log(Status.INFO, "Click on Lazy to Cook Button");
			foodRecommend.clickonLazyToCook();
			Thread.sleep(2000);
			/**
			 * Get new window redirected URLss
			 */
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
				if (driver.getCurrentUrl().contains("swiggy")) {
					child.log(Status.PASS, "Redirected to Swiggy.Com");
					child.log(Status.INFO, String.format("handle: %s, url: %s", handle, driver.getCurrentUrl()));
					child.log(Status.INFO, "Lazy to Cook button has redirected successfully to Swiggy.com");
				}
			}
		} else {
			child.log(Status.FAIL, "No Results Found");
		}
		Thread.sleep(2000);
		child.log(Status.INFO, "Switching Back to the parent Window");
		driver.switchTo().window(parent);
		// Click on Second Option
		child.log(Status.INFO, "Clicking on the another option available");
		foodRecommend.clickSecondOption();
	}

	/**
	 * Method to Cook Your Food Based on Your Preferences
	 *
	 * @throws Exception
	 */
	@Test
	public void cookYourFoodTest() throws Exception {
		foodRecommend = new FoodRecommendationPage();
		foodRecommend = PageFactory.initElements(driver, FoodRecommendationPage.class);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		child = ExtentTestManager.startTest("Order your food using Food Recommendation", "Order Your Food");
		foodRecommend.waitForLoad(driver);
		child.log(Status.INFO, "Select Cuisine on Select Cuisine Page");
		foodRecommend.SelectCuisine(cuisine);
		Thread.sleep(2000);
		child.log(Status.INFO, "Select Your Food Preferences");
		foodRecommend.SelectFoodPreference(preference);
		Thread.sleep(2000);
		child.log(Status.INFO, "Click on Search Button");
		foodRecommend.clickOnSearchButton();
		Thread.sleep(5000);
		child.log(Status.INFO, "Redirecting to the next page");
		if (driver.getPageSource().contains("Food results based on your choice:")) {
			child.log(Status.INFO, "Click on Love to Cook Button");
			foodRecommend.clickonLoveToCook();
			Thread.sleep(2000);
			for (String handle : driver.getWindowHandles()) {
				driver.switchTo().window(handle);
				System.out.println(String.format("handle: %s, url: %s", handle, driver.getCurrentUrl()));
				if (driver.getCurrentUrl().contains("dish")) {
					/**
					 * Click on Steps button
					 */
					child.log(Status.INFO, "Click on Steps Button");
					Thread.sleep(2000);
					foodRecommend.clickonSteps();
					Thread.sleep(2000);
					// child.log(Status.INFO, "Click on Order Something Similar Button");
					// foodRecommend.clickOnOrderSimilar();
					// for (String handle1 : driver.getWindowHandles()) {
					// driver.switchTo().window(handle);
					// if (driver.getCurrentUrl().contains("swiggy")) {
					// child.log(Status.PASS, "Redirected to Swiggy.Com");
					// child.log(Status.INFO , String.format("handle: %s, url: %s",
					// handle1,driver.getCurrentUrl()));
					// child.log(Status.INFO , "Order Something Similar has redirected successfully
					// to Swiggy.com");
					// }
					// }
				} else {
					child.log(Status.FAIL, "No Results Found");
				}
				child.log(Status.PASS, "Test Case Passed for Love To Cook");
			}
		}

	}

	/**
	 * Method to Login
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void CheckLogin() throws InterruptedException {
		foodRecommend = new FoodRecommendationPage();
		foodRecommend = PageFactory.initElements(driver, FoodRecommendationPage.class);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		child = ExtentTestManager.startTest("Login into the WebApp", "Order Your Food");
		foodRecommend.waitForLoad(driver);
		child.log(Status.INFO, "Click on Login Button");
		foodRecommend.clickonLogin();
		Thread.sleep(3000);
		if (driver.getPageSource().contains(" We are cooking some good features for you. ")) {
			child.log(Status.PASS, "Login Redirected Successfully ,Going under maintenance");
		} else {
			child.log(Status.FAIL, "Login Did Not Redirected Successfully");
		}

	}

	/**
	 * Method to Sign Up
	 * 
	 * @throws InterruptedException
	 */
	@Test
	public void CheckSignUp() throws InterruptedException {
		foodRecommend = new FoodRecommendationPage();
		foodRecommend = PageFactory.initElements(driver, FoodRecommendationPage.class);
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		child = ExtentTestManager.startTest("Sign Up into WebApp", "Order Your Food");
		foodRecommend.waitForLoad(driver);
		child.log(Status.INFO, "Click on Sign Up Button");
		foodRecommend.clickonSignUp();
		Thread.sleep(3000);
		if (driver.getPageSource().contains(" We are cooking some good features for you. ")) {
			child.log(Status.PASS, "Sign Up Redirected Successfully ,Going under maintenance");
		} else {
			child.log(Status.FAIL, "Sign Up Did Not Redirected Successfully");
		}

	}
	
	
	 /**
	 * Method to test for No Data
	 * @throws InterruptedException
	 */
	
	 @Test
	 public void NoDataVerification() throws InterruptedException
	 {
	 foodRecommend = new FoodRecommendationPage();
	 foodRecommend = PageFactory.initElements(driver,
	 FoodRecommendationPage.class);
	 driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
	 child = ExtentTestManager.startTest("Test for No Dishes found", "Order Your Food");
	 foodRecommend.waitForLoad(driver);
	 child.log(Status.INFO, "Select Mexican Cuisine on Select Cuisine Page");
	 foodRecommend.SelectCuisine("mexican");
	 Thread.sleep(2000);
	 child.log(Status.INFO, "Select Your Food Preferences");
	 foodRecommend.SelectFoodPreference("veg");
	 Thread.sleep(2000);
	 foodRecommend.clickOnSearchButton();
	 Thread.sleep(5000);
	 if (driver.getPageSource().contains(" We didn't find any dishes for cooking. ")) {
			child.log(Status.FAIL, "No Dishes Found For the User");
			foodRecommend.clickonBackToHomeButton();
			Assert.fail();
	} 
	 }

}
