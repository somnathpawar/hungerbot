import json
import os
import boto3
from boto3.dynamodb.conditions import Key

# grab environment variables
RUNTIME = boto3.client('runtime.sagemaker')
DYNAMODB_TABLE = os.environ['DYNAMODB_TABLE']
dynamodb = boto3.resource('dynamodb')

def getDishDetails(event, context):
    headers = {
        "Access-Control-Allow-Origin": "*"
    }

    id = event['pathParameters']['id']
    if id == "":  raise Exception('Invalid id!')

    table = dynamodb.Table(DYNAMODB_TABLE)
    result = table.query(
        KeyConditionExpression=Key('Id').eq(str(id))
    )

    response = {
        "statusCode": 200,
        "headers": headers,
        "body": json.dumps(result['Items'])
    }

    return response
