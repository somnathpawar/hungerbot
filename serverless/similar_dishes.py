import json
import os
import boto3
from boto3.dynamodb.conditions import Key
import csv

# grab environment variables
ENDPOINT_NAME = os.environ['ENDPOINT_NAME']
RUNTIME = boto3.client('runtime.sagemaker')
DYNAMODB_TABLE = os.environ['DYNAMODB_TABLE']
dynamodb = boto3.resource('dynamodb')

def getSimilarDishes(event, context):
    headers = {
        "Access-Control-Allow-Origin": "*"
    }

    id = event['pathParameters']['id']
    if id == "":  raise Exception('Invalid id!')

    # Send request to sagemaker api
    data = json.loads(json.dumps(event))
    payload = str(id)
    sagemakerResponse = RUNTIME.invoke_endpoint(
        EndpointName=ENDPOINT_NAME,
        ContentType='text/csv',
        Body=payload,
        Accept='Accept'
        )
    sagemakerResult = sagemakerResponse['Body'].read().decode('ascii')
    sagemakerResultArray = sagemakerResult.split("\n")
    sagemakerResultArray = sagemakerResultArray[:(len(sagemakerResultArray)-1)]
    sagemakerResultArray = [int(i) for i in sagemakerResultArray]

    table = dynamodb.Table(DYNAMODB_TABLE)
    body = []
    for value in sagemakerResultArray:
        result = table.query(
                KeyConditionExpression=Key('Id').eq(str(value))
            )
        if result['Count'] > 0:
            body.append(result['Items'][0])
    
    response = {
        "statusCode": 200,
        "headers": headers,
        "body": json.dumps(body)
    }

    return response
