import json
import os
import boto3
import random
from boto3.dynamodb.conditions import Key, Attr
dynamodb = boto3.resource('dynamodb')

DEFAULT_LIMIT = 10

def getDishes(event, context):
    body = json.loads(event['body'])
    allowedFilters = {
        'cuisine' : "Cuisine",
        'preference' : "Preference"
    }

    criteria = ''
    table = dynamodb.Table(os.environ['DYNAMODB_TABLE'])

    for key, value in allowedFilters.items():
        if key in body:
            if criteria == '':
                criteria = Attr(value).eq(body[key].lower())
            else:
                criteria = criteria & Attr(value).eq(body[key].lower())

    # fetch todo from the database
    if criteria == '':
        result = table.scan(Limit=DEFAULT_LIMIT)
    else:
        result = table.scan(
            FilterExpression = criteria
        )

    items = result['Items']
    # comment on/off this line to toggle shuffle on results
    #random.shuffle(items)
    # create a response
    response = {
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Origin": "*"
        },
        "body": json.dumps(items)
    }

    return response
