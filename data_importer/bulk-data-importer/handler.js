'use strict';

const AWS = require('aws-sdk');
const s3 = new AWS.S3();
const docClient = new AWS.DynamoDB.DocumentClient({
    region: 'us-east-1'
});
const response = {
    statusCode: 200,
    body: JSON.stringify('CSV to DynamoDB data import is success!'),
};

module.exports.importRecipes = (event, context) => {
    const bucketName = process.env.S3_BUCKET_NAME;
    const keyName = process.env.S3_KEY;
    const params = {
        Bucket: bucketName,
        Key: keyName
    };
    const csv = require('csvtojson');
    //grab the csv file from s3        
    const s3Stream = s3.getObject(params).createReadStream()
    csv().fromStream(s3Stream)
        .on('data', (row) => {
            //read each row 
            let jsonContent = JSON.parse(row);
            console.log(JSON.stringify(jsonContent));
            //push each row into DynamoDB
            let paramsToPush = {
                TableName: process.env.DYNAMODB_TABLE_NAME,
                Item: {
                    "Id": jsonContent.id,
                    "Name": jsonContent.name,
                    "RecipeId": jsonContent.recipe_id,
                    "Steps": jsonContent.steps,
                    "Description": jsonContent.description,
                    "Ingredients": jsonContent.ingredients,
                    "RecipePhotoURL": jsonContent.recipe_photo_url,
                    "Cuisine": jsonContent.cuisine,
                    "Preference": jsonContent.preference
                }
            };
            addData(paramsToPush);
        });

        return response;
};
function addData(params) {
    console.log("Adding a new item based on: ");
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            console.log("Added item:", JSON.stringify(params.Item, null, 2));
        }
    });
}