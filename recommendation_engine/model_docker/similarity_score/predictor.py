from __future__ import print_function

import os
import json
import pickle
from io import StringIO
import sys
import signal
import traceback
import numpy

import flask

import pandas as pd
import pickle

prefix = '/opt/ml/'
model_path = os.path.join(prefix, 'model')

reference = pd.read_csv(os.path.join(prefix, 'model', 'final_upload.csv'))
indices = pd.Series(reference.recipe_id, index = reference['Id'])
indices = indices[~indices.isnull()]

with open(os.path.join(model_path, 'similarity_finder.pickle'), 'rb') as inp:
    predicor = pickle.load(inp)


def get_recommendations(index, cosine_sim):
    idx = index
    sim_scores = list(enumerate(cosine_sim[idx]))
    sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    sim_scores = sim_scores[1:3]
    dish_indices = [i[0] for i in sim_scores]
    return dish_indices

# A singleton for holding the model. This simply loads the model and holds it.
# It has a predict function that does a prediction based on the model and the input data.
class ScoringService(object):
    model = None                # Where we keep the model when it's loaded
    @classmethod
    def get_model(cls):
        """Get the model object for this instance, loading it if it's not already loaded."""
        
        cls.model = predicor
        return cls.model

    @classmethod
    def predict(cls, input):
        """For the input, do the predictions and return them.

        Args:
            input (a pandas dataframe): The data on which to do the predictions. There will be
                one prediction per row in the dataframe"""
        clf = cls.get_model()
        val = int(input.iloc[0].values)        
        res = get_recommendations(val, clf)
        print(res)
        return res

# The flask app for serving predictions
app = flask.Flask(__name__)

@app.route('/ping', methods=['GET'])
def ping():
    health = ScoringService.get_model() is not None  # You can insert a health check here

    status = 200 if health else 404
    return flask.Response(response='\n', status=status, mimetype='application/json')

@app.route('/invocations', methods=['POST'])
def transformation():
    data = None

    # Convert from CSV to pandas
    if flask.request.content_type == 'text/csv':
        data = flask.request.data.decode('utf-8')
        s = StringIO(data)
        data = pd.read_csv(s, header = None)
    else:
        return flask.Response(response='This predictor only supports CSV data', status=415, mimetype='text/plain')

    print('Invoked with {} records'.format(data.shape[0]))

    # Do the prediction
    predictions = ScoringService.predict(data)

    # Convert from numpy back to CSV
    out = StringIO()
    pd.DataFrame({'results':predictions}).to_csv(out, index=False, header = None)
    result = out.getvalue()

    return flask.Response(response=result, status=200, mimetype='text/csv')
